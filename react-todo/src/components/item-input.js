import React, { Component } from 'react';

class itemInput extends Component {
  render() {
    return (
      <div className="user-input">
        <h1 className="title">To-do's</h1>
        <input className="input" type="text" />
        <button className="button">Apply</button>
      </div>
    );
  }
}

export default itemInput;