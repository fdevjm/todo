import Vue from 'vue';
import VueFire from 'vuefire';
import Firebase from 'firebase';

Vue.use(VueFire);

const config = {
  apiKey: 'AIzaSyCZ4xM4uUvYe2_67zwJyNfv_1xs7K_ZpTg',
  authDomain: 'vue-todo-73107.firebaseapp.com',
  databaseURL: 'https://vue-todo-73107.firebaseio.com',
  projectId: 'vue-todo-73107',
  storageBucket: 'vue-todo-73107.appspot.com',
  messagingSenderId: '437519896131',
};
const firebaseApp = Firebase.initializeApp(config);
const firebaseDB = firebaseApp.database();

export default firebaseDB;
